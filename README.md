# Secure Text Editor

Secure Text Editor is a project which enables users to store text files in encrypted forms using a passphrase for symmetric key encryption. This is as much a project to implement tkinter (python) as it is to enable a secured and personal log of activities, with privacy considerations to minimize oversight

## Installation
This can be run command line and will function just fine. It can also be built into an exe by using the following command (with pyinstaller installed)
```pyinstaller --icon=icon.ico -F --noconsole --add-data "icon.ico;." ./textedit.py```
The result of this can also just be found in the dist/ folder

## Usage
Run textedit.py either at the command line or by clicking on the File (if python is configured). It will allow you to start new files or open existing encrypted files. Once a passphrase is entered correctly, the data will be decrypted and can be edited. passphrase will not need to be re-entered if file is merely edited or viewed. Passphrase will be required if new files are saved or if a file is being saved under a new name, or when unlocking a session.

Users can lock their text editor in the Menu or by pressing Ctrl+L in the text area. This will replace all text with "FILE LOCKED//FILE LOCKED" spam and request the passphrase to unlock again. If an incorrect passphrase is provided, user will be given the option to exit the file without saving and a new file is opened for editing.

## Support
Additional requests for information and bugs can be directed to the author. I don't anticipate additional functionality to be of significant value.

## Authors and acknowledgment
Work collated and interconnected by [williamsc](https://gitlab.com/williamsc). Fundamental bits of the Text Editor were largely taken from a CodeSpeedy tutorial as well as an exceptionally written [StackOverflow post by Martijn Pieters](https://stackoverflow.com/questions/2490334/simple-way-to-encode-a-string-according-to-a-password)

