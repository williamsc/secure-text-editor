from encrypt import password_decrypt, password_encrypt
import tkinter
import cryptography
from tkinter import messagebox, Tk, filedialog, simpledialog, StringVar, Label, Menu, GROOVE, TOP, BOTH, BOTTOM, VERTICAL, RIGHT, END, Y, X, Scrollbar, Text
import os, sys

class TextEditor:
  def __init__(self,root):
    self.root = root
    self.root.title("Cryptographic Text Editor")
    self.root.geometry("1200x700+200+150")
    self.filename = None
    self.title = StringVar()
    self.status = StringVar()
    self.titlebar = Label(self.root,textvariable=self.title,font=("times new roman",15,"bold"),bd=2,relief=GROOVE)
    self.titlebar.pack(side=TOP,fill=BOTH)
    self.settitle()
    self.statusbar = Label(self.root,textvariable=self.status,font=("times new roman",15,"bold"),bd=2,relief=GROOVE)
    self.statusbar.pack(side=BOTTOM,fill=BOTH)
    self.status.set("Welcome to el encrypto")
    self.menubar = Menu(self.root,font=("times new roman",15,"bold"),activebackground="#00061a", background="#00061a")
    self.root.config(menu=self.menubar)
    self.initial_dir = "./output"
    # Creating File Menu
    self.filemenu = Menu(self.menubar,font=("times new roman",12,"bold"),activebackground="skyblue",tearoff=0)
    self.filemenu.add_command(label="New",accelerator="Ctrl+N",command=self.newfile)
    self.filemenu.add_command(label="Open",accelerator="Ctrl+O",command=self.openfile)
    self.filemenu.add_command(label="Save",accelerator="Ctrl+S",command=self.savefile)
    self.filemenu.add_command(label="Save As",accelerator="Ctrl+A",command=self.saveasfile)
    self.filemenu.add_command(label="Lock File",accelerator="Ctrl+L",command=self.lockfile)
    self.filemenu.add_command(label="Settings",command=self.settings)
    self.filemenu.add_separator()
    self.filemenu.add_command(label="Exit",accelerator="Ctrl+E",command=self.exit)
    self.menubar.add_cascade(label="File", menu=self.filemenu)
    # Creating Edit Menu
    self.editmenu = Menu(self.menubar,font=("times new roman",12,"bold"),activebackground="skyblue",tearoff=0)
    self.editmenu.add_command(label="Cut",accelerator="Ctrl+X",command=self.cut)
    self.editmenu.add_command(label="Copy",accelerator="Ctrl+C",command=self.copy)
    self.editmenu.add_command(label="Paste",accelerator="Ctrl+V",command=self.paste)
    self.editmenu.add_separator()
    self.editmenu.add_command(label="Undo",accelerator="Ctrl+U",command=self.undo)
    self.menubar.add_cascade(label="Edit", menu=self.editmenu)
    # Creating Help Menu
    self.helpmenu = Menu(self.menubar,font=("times new roman",12,"bold"),activebackground="skyblue",tearoff=0)
    self.helpmenu.add_command(label="About",command=self.infoabout)
    self.menubar.add_cascade(label="Help", menu=self.helpmenu)
    # Creating Text Area
    scrol_y = Scrollbar(self.root,orient=VERTICAL)
    self.txtarea = Text(self.root,yscrollcommand=scrol_y.set,font=("courier new",12,"bold"),state="normal",relief=GROOVE, bg="#00061a", fg="#fff5e6", insertbackground="#fff5f6")
    scrol_y.pack(side=RIGHT,fill=Y)
    scrol_y.config(command=self.txtarea.yview)
    self.txtarea.pack(fill=BOTH,expand=1)
    # Calling shortcuts funtion
    self.shortcuts()
  
  
  def settitle(self):
    self.title.set(self.filename or "Untitled")


  def newfile(self,*args):
    self.txtarea.delete("1.0",END)
    self.filename = None
    self.settitle()
    self.status.set("New File Created")         
  

  def openfile(self,*args):
    try:
      self.filename = filedialog.askopenfilename(title = "Select file",filetypes = (("All Files","*.*"),("Text Files","*.txt"),("Python Files","*.py")), initialdir=self.initial_dir, parent=self.root)
      if self.filename:
        f = open(self.filename,'r')
        encrfile = ''.join(f.readlines()).encode()
        self.passphrase = ""
        dialogtext = 'Passphrase:'
        while True:
          self.passphrase = tkinter.simpledialog.askstring('Passphrase to unlock', f"{dialogtext}\t\t\t\t\t\t\t\t\t\t\t\t", show="*", parent=self.root)
          try:
            # User closed passphrase dialog
            if self.passphrase is None:
              return
            password_decrypt(encrfile, self.passphrase)
            break
          except cryptography.fernet.InvalidToken:
            dialogtext = "Incorrect passphrase. Please submit new passphrase:"
            pass
        infile = password_decrypt(encrfile, self.passphrase).decode()
        self.txtarea.delete("1.0",END)
        self.txtarea.insert(END,infile)
        f.close()
        self.settitle()
        self.status.set("Opened Successfully")
    except Exception as e:
      messagebox.showerror("Exception",e)


  def savefile(self,*args):
    try:
      if self.filename:
        data = self.txtarea.get("1.0",END)
        token = password_encrypt(data.encode(), self.passphrase)
        outfile = open(self.filename,"w")
        outfile.write(token.decode("utf-8"))
        outfile.close()
        self.settitle()
        self.status.set("Saved Successfully")
      else:
        self.saveasfile()
    except Exception as e:
      messagebox.showerror("Exception",e)


  def saveasfile(self,*args):
    try:
      untitledfile = filedialog.asksaveasfilename(title = "Save file As",defaultextension=".ts",initialfile = "Untitled.txt",filetypes = (("All Files","*.*"),("Text Files","*.txt"),("Python Files","*.py")), initialdir=self.initial_dir, parent=self.root)
      # Checking these separately to avoid asking for both if user quits from file selector
      if untitledfile != "":
        passphrase = tkinter.simpledialog.askstring("Passphrase to unlock", "Passphrase\t\t\t\t\t\t\t\t\t\t\t\t", show="*", parent=self.root)
        if passphrase is not None:
          data = self.txtarea.get("1.0",END)
          token = password_encrypt(data.encode(), passphrase)
          outfile = open(untitledfile,"w")
          outfile.write(token.decode("utf-8"))
          outfile.close()
          self.passphrase = passphrase
          self.filename = untitledfile
          self.settitle()
          self.status.set("Saved Successfully")
    except Exception as e:
      messagebox.showerror("Exception",e)


  def lockfile(self,*args):
      if self.filename is not None:
        try:
          self.lockeddata = self.txtarea.get("1.0",END)
          self.txtarea.delete("1.0",END)
          self.txtarea.insert(END,"//".join(("FILE-LOCKED "*500).split()))
          while True:
            passphrase = tkinter.simpledialog.askstring("Passphrase to unlock", "Passphrase\t\t\t\t\t\t\t\t\t\t\t\t", show="*", parent=self.root)
            if passphrase == self.passphrase:
              self.txtarea.delete("1.0",END)
              self.txtarea.insert(END,self.lockeddata)
              return
            quitcheck = messagebox.askyesno("INCORRECT PASSWORD", "Incorrect passphrase. Would you like to close this document? \r\nAny unsaved progress will be lost\t\t\t\t\t\t\t\t\t\t\t\t", parent=self.txtarea)
            if quitcheck:
              return self.newfile()
        except Exception as e:
          messagebox.showerror("Exception",e)


  def exit(self,*args):
    op = messagebox.askyesno("WARNING","Your Unsaved Data May be Lost!!")
    if op>0:
      self.root.destroy()
    else:
      return


  def cut(self,*args):
    self.txtarea.event_generate("<<Cut>>")


  def copy(self,*args):
          self.txtarea.event_generate("<<Copy>>")


  def paste(self,*args):
    self.txtarea.event_generate("<<Paste>>")


  def settings(self,*args):
    new_dir = filedialog.askdirectory(parent=self.root)
    if len(new_dir)>0:
      self.initial_dir = new_dir

  def undo(self,*args):
    try:
      self.txtarea.delete("1.0",END)
      if self.filename:
        infile = open(self.filename,"r")
        for line in infile:
          self.txtarea.insert(END,line)
        infile.close()
      else:
        self.txtarea.delete("1.0",END)
      self.settitle()
      self.status.set("Undone Successfully")
    except Exception as e:
      messagebox.showerror("Exception",e)


  def infoabout(self):
    messagebox.showinfo("About Text Editor","A Simple Text Editor\nCreated using Python.\nPivoted/refactored from a CodeSpeedy tutorial and some encryption sources to become a password protected text editor")


  def shortcuts(self):
    self.txtarea.bind("<Control-n>",self.newfile)
    self.txtarea.bind("<Control-o>",self.openfile)
    self.txtarea.bind("<Control-s>",self.savefile)
    self.txtarea.bind("<Control-a>",self.saveasfile)
    self.root.bind("<Control-l>",self.lockfile)
    self.txtarea.bind("<Control-e>",self.exit)
    self.txtarea.bind("<Control-x>",self.cut)
    self.txtarea.bind("<Control-c>",self.copy)
    self.txtarea.bind("<Control-v>",self.paste)
    self.txtarea.bind("<Control-u>",self.undo)

datafile = 'icon.ico'
def resource_path(relative_path):    
  try:       
    base_path = sys._MEIPASS
  except Exception:
    base_path = os.path.abspath(".")
  return os.path.join(base_path, relative_path)

def main():
    root = Tk()
    root.iconbitmap(default=resource_path(datafile))
    TextEditor(root)
    root.mainloop()


if __name__ == "__main__":
    main()